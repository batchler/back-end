<p align="center"><a href="https://api.depaepejonathan.be/" target="_blank"><img src="https://i.imgur.com/STJi0e0.png" width="400"></a></p>

# Batchler - Scory Feedback - API POC

[![Generic badge](https://img.shields.io/badge/Version-Alpha-red.svg)](https://shields.io/)


## Useful links

- Web app: https://batchler.depaepejonathan.be/
- API : https://api.depaepejonathan.be/

## How to run the api locally

### Required
-  Connection to the Database [mysql](https://www.mysql.com/downloads/)
-  Latest version of [NodeJs](https://nodejs.org/en/download/)


### Installing

- Clone the server into your preferred folder
  - `git clone https://gitlab.com/batchler/back-end.git`
- Install the packages
  - `npm install`
- Run the Web app
  -  `npm run start`

##
### Usefull packages to know
- puppeteer
  - Creating a virtual browser and taking a screenshot
- decompress
  - Unzip the zip files
- css-validator
  - Validates css files with api
- html-validator
  - Validates html files with api



## Known bugs
- Currently, no bugs found

## Credentials
Depaepe Jonathan
