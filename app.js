let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');
const cors = require('cors');

let exerciseRouter = require('./routes/exercise');
let validateRouter = require('./routes/validate');
let studentRouter = require('./routes/student')
const dotenv = require('dotenv');

dotenv.config();

let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());


app.use('/api/exercise', exerciseRouter);
app.use('/api/validate', validateRouter);
app.use('/api/students', studentRouter);


module.exports = app;
