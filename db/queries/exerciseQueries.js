const connection = require('../db');

const createQuery = 'INSERT INTO exercises (name,slug, path, fonts, colors, banned, include) VALUES (?, ?, ?, ?, ?, ?, ?)';
const getQuery = 'SELECT * FROM exercises';
const getQueryWithSlug = 'SELECT * FROM exercises where slug = ?';
const getQueryById= 'SELECT * FROM exercises where id = ?';
const updateQuery = 'UPDATE exercises SET name = ?, path = ?, fonts = ?, colors = ?, banned = ?, include = ? WHERE id = ?';

function createExercise(exercise, cb) {
    const values = [exercise.name, exercise.slug, exercise.path, JSON.stringify(exercise.fonts), JSON.stringify(exercise.colors), JSON.stringify(exercise.banned), JSON.stringify(exercise.include)];
    connection.query(createQuery, values, (error, results) => {
        if (error) {
            console.error('Error creating exercise:', error);
            return cb(error, null);
        }

        exercise.id = results.insertId;
        cb(null, exercise)
    });
}

function getExercises(cb) {
    connection.query(getQuery, (error, results) => {
        if (error) {
            connection.end();
            return cb(error, null);
        }
        return cb(null, results);
    });
}

function getExercisesBySlug(slug,cb) {
    connection.query(getQueryWithSlug, [slug],(error, results) => {
        if (error) {
            return cb(error, null);
        }
        return cb(null, results);
    });
}

function getExercisesById(id,cb) {
    connection.query(getQueryById, [id],(error, results) => {
        if (error) {
            return cb(error, null);
        }
        return cb(null, results);
    });
}

function updateExercise(id, exercise, cb) {
    connection.query(updateQuery, [exercise.name, exercise.slug, exercise.path, JSON.stringify(exercise.fonts), JSON.stringify(exercise.colors), JSON.stringify(exercise.banned), JSON.stringify(exercise.include), id], (error) => {
        if (error) {
            console.error('Error updating  exercise:', error);
            return cb(error);
        }
        cb(null);
    });
}


module.exports = {
    createExercise,
    getExercises,
    getExercisesBySlug,
    getExercisesById,
    updateExercise

}
