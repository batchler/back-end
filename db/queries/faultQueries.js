const connection = require("../db");


const getQueryByValidation = 'select * from fault where validationID=?';
const createQuery = 'INSERT INTO fault (validationID, message, penalty, file, line) VALUES (?, ?, ?, ?, ?)';

function getFaultByValidation(validationID,cb) {
    connection.query(getQueryByValidation,[validationID] ,(error, results) => {
        if (error) {
            return cb(error, null);
        }
        return cb(null, results);
    });
}

function createFault(validationID, message,penalty,file,line) {
    connection.query(createQuery,[validationID, message, penalty, file, line], (error, results) => {
        if (error) {
            console.log(error)
        }
    });
}

module.exports ={
    getFaultByValidation,
    createFault
}