const connection = require("../db");

const createQuery = 'INSERT INTO plagiarism (validationID, filename, comparedID, comparedFilename, percentage) VALUES (?, ?, ?, ?, ?)';
const getQueryByValidationID = 'SELECT * FROM plagiarism where validationID = ? or comparedID = ?';

function createPlagiarism(validationID,filename, comparedID, comparedFilename, percentage) {
    connection.query(createQuery, [validationID, filename, comparedID, comparedFilename, percentage], (error, results) => {
        if (error) {
            console.error('Error creating plagiarism:', error);
        }
    });
}

function getPlagiarismsByValidationID(validationID, cb) {
    connection.query(getQueryByValidationID,[validationID, validationID], (error, results) => {
        if (error) {
            connection.end();
            return cb(error, null);
        }
        return cb(null, results);
    });
}

module.exports = {
    createPlagiarism,
    getPlagiarismsByValidationID
}