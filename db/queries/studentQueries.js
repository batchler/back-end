const connection = require("../db");


const getQuery = 'SELECT * FROM student';
const getQueryByNane = 'SELECT * FROM student  WHERE LOWER(firstname) = LOWER(?) AND LOWER(lastname) = LOWER(?);';
const getQueryById = 'SELECT * FROM student  WHERE id=?';
function getStudents(cb) {
    connection.query(getQuery, (error, results) => {
        if (error) {
            connection.end();
            return cb(error, null);
        }
        return cb(null, results);
    });
}

function getStudentByName(firstname, lastname, cb) {
    connection.query(getQueryByNane,[firstname, lastname], (error, results) => {
        if (error) {
            connection.end();
            return cb(error, null);
        }
        return cb(null, results);
    });
}

function getStudentById(studentId, cb){
    connection.query(getQueryById,[studentId], (error, results) => {
        if (error) {
            connection.end();
            return cb(error, null);
        }
        return cb(null, results);
    });
}

module.exports ={
    getStudents,
    getStudentByName,
    getStudentById
}