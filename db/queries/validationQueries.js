const connection = require("../db");



const createQuery = 'INSERT INTO validation (path, date, score, exerciseID, userID) VALUES (?, ?, ?, ?, ?)';
const getQueryByExercise = 'SELECT * FROM validation where exerciseID = ?';
const getQueryById = 'SELECT * FROM validation where id = ?';
const updateQueryScore = 'UPDATE validation SET score = ? WHERE id = ?;';

function createValidation(path,score, exerciseID, userID, cb) {
    const date = new Date();

    connection.query(createQuery, [path, date, score, exerciseID, userID], (error, results) => {
        if (error) {
            console.error('Error creating exercise:', error);
            return cb(error, null);
        }

        cb(null, results.insertId)
    });
}

function getValidationsByExercise(exerciseID, cb) {
    connection.query(getQueryByExercise,[exerciseID], (error, results) => {
        if (error) {
            connection.end();
            return cb(error, null);
        }
        return cb(null, results);
    });
}

function getValidationById(validationID ,cb){
    connection.query(getQueryById,[validationID], (error, results) => {
        if (error) {
            connection.end();
            return cb(error, null);
        }
        return cb(null, results);
    });
}

function updateValidationScore(id, score, cb){
    connection.query(updateQueryScore, [score, id], (error, results) => {
        if (error) {
            console.error('Error creating exercise:', error);
            return cb(error, null);
        }
        cb(null, results.insertId)
    });
}

module.exports = {
    createValidation,
    getValidationsByExercise,
    getValidationById,
    updateValidationScore
}