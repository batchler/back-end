let express = require('express');
let router = express.Router();
const os = require('os');
const multer  = require('multer');
const {uploadExercise,createExercise,getExercisesById, updateExercise, getExercises} = require("../services/exercise/exerciseService");
const {validateExercise, validate} = require("../validation/validations");
const upload = multer({ dest: os.tmpdir() });


router.get('/', getExercises);

router.get("/:id", getExercisesById)

router.post('/upload', upload.single('file'), uploadExercise);

router.post('/', validateExercise, validate , createExercise);

router.put('/:id', validateExercise, validate , updateExercise);

module.exports = router;
