let express = require('express');
let router = express.Router();
const {getStudents} = require('../services/student/studentservice')


router.get('/', getStudents);


module.exports = router;
