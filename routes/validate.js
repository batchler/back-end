let express = require('express');
let router = express.Router();
const os = require('os');
const multer = require('multer');
const {validateExercise} = require('../services/validate/validate')
const upload = multer({dest: os.tmpdir()});

router.post('/', upload.single('file'), validateExercise);


module.exports = router;
