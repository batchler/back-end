const exerciseDB = require('../../db/queries/exerciseQueries');
const decompress = require("decompress");
const fs = require("fs");
const cssJson = require("cssjson");
const {createScreenshots} = require("../../utils/visualRegression");
const {findIndexHtml} = require("../../utils/htmlFinder");
const {getValidationsByExercise} = require("../../db/queries/validationQueries");
const {getFaultByValidation} = require("../../db/queries/faultQueries");
const plagiarism = require("../../utils/plagiarism");
const {getStudentById} = require("../student/studentservice");


function uploadExercise(req, res){
    decompress(req.file.path, `dist/exercise`).then(async files => { // add "/${fileName}" on windows
        const cssWithFonts = {};
        const cssWithColors = {};
            for (let file of files) {
                if (file.type === 'file') {
                    if (file.path.includes('.css') && !file.path.includes("reset.css")) {
                        const fileContent  = fs.readFileSync(__dirname + `/../../dist/exercise/${file.path}`, 'utf8')
                        const css = cssJson.toJSON(fileContent);
                        const data = css.children;
                        for (const selector in data) {
                            const properties = data[selector];
                            for (const attribute in properties.attributes) {
                                if (attribute.includes('font-family') || attribute.includes('font-size')) {
                                    cssWithFonts[selector] = properties.attributes[attribute];
                                }
                            }
                            for (const attribute in properties.attributes) {
                                if (attribute === 'color') {
                                    cssWithColors[selector] = properties.attributes[attribute];
                                }
                            }
                        }

                    }
                }
            }
        res.json({
            path:`/dist/exercise/${req.file.originalname.replace(".zip", "")}`,
            fonts: cssWithFonts,
            colors: cssWithColors
        });

        }

    )

}

function createExercise(req, res){
    const {name,slug, path, fonts, colors, banned, include} = req.body;
    const exercise = {name, slug, path, fonts, colors, banned, include};
    const indexPath = findIndexHtml(`${__dirname}/../../${path}`);
    createScreenshots(`${indexPath}`,`${__dirname}/../../public/solutions/${slug}/`, true)
    exerciseDB.createExercise(exercise, (er, result)=>{
        if(er){
            console.error('Error creating exercise:', er);
            res.status(500).json({ error: 'Internal Server Error' });
        }else{
            res.json(result);
        }
    })
}



function getExercises(req, res){
    exerciseDB.getExercises((error, results) => {
        if (error) {
            console.error('Error getting exercises:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            const exercises = results.map((exercise) => ({
                ...exercise,
                fonts: JSON.parse(exercise.fonts),
                colors: JSON.parse(exercise.colors),
                banned: JSON.parse(exercise.banned),
                include: JSON.parse(exercise.include),
            }));
            res.json(exercises);
        }
    });
}

function updateExercise(req, res){
    const {name, slug, path, fonts, colors, banned, include} = req.body;
    const exercise = {name, slug, path, fonts, colors, banned, include};
    exerciseDB.updateExercise(req.params.id, exercise, (error) => {
        if (error) {
            console.error('Error getting exercises:', error);
            res.status(500).json({ error: 'Internal Server Error' });
        } else {
            exercise.id = req.params.id
            res.json(exercise);
        }
    });
}

async function getExerciseBySlug(slug){
    return new Promise((resolve, reject) => {
        exerciseDB.getExercisesBySlug(slug, (err, result) => {
            if (err) {
                console.log("error", err);
                reject(false);
            } else {
                console.log("result", result.length > 0);
                resolve(result[0]);
            }
        });
    });
}

function getExercisesById(req, res){
    let returnBody = {};
    console.log("here")
    exerciseDB.getExercisesById(req.params.id, (err, exercise)=> {
        returnBody = exercise[0];
        returnBody.validations = [];
        getValidationsByExercise(req.params.id, async (errVal, validations) => {
            for (let validation of validations) {
                let validationBody = validation;
                getFaultByValidation(validation.id, (err, faults) => {
                    console.log(err)
                    validationBody.warnings = faults;
                })
                validationBody.student = await getStudentById(validation.userID)
                validationBody.plagiarism = await plagiarism.getProjectPlagiarism(validation.id);
                const id = validation.path.split('/').splice(2, 1).join('/');
                validationBody.images = [{
                    submittedImage: `/students/${id}/screenshot_360x460_student.png`,
                    solutionImage: `/solutions/${returnBody.slug}/screenshot_360x460_solution.png`,
                    resolution: "360x460"
                },
                    {
                        submittedImage: `/students/${id}/screenshot_768x1024_student.png`,
                        solutionImage: `/solutions/${returnBody.slug}/screenshot_768x1024_solution.png`,
                        resolution: "768x1024"
                    },
                    {
                        submittedImage: `/students/${id}/screenshot_1366x768_student.png`,
                        solutionImage: `/solutions/${returnBody.slug}/screenshot_1366x768_solution.png`,
                        resolution: "1366x768"
                    },
                    {
                        submittedImage: `/students/${id}/screenshot_1920x1080_student.png`,
                        solutionImage: `/solutions/${returnBody.slug}/screenshot_1920x1080_solution.png`,
                        resolution: "1920x1080"
                    }]
                returnBody.validations.push(validationBody)
            }
            res.json(returnBody)
        })

    })
}


module.exports = {uploadExercise, createExercise, getExercises, updateExercise, getExerciseBySlug, getExercisesById};
