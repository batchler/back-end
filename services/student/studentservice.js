const studentDB = require("../../db/queries/studentQueries")

function getStudents(req, res){
    console.log("triggered")
    studentDB.getStudents((err, result) => {
      if (err){
          console.log(err)
          res.status(500).send("Could nog get the students")
      }
      res.json(result)

    })
}

async function getStudentByName(firstname, lastname){
    return new Promise((resolve, reject) => {
        studentDB.getStudentByName(firstname,lastname, (err, result) => {
            if (err) {
                reject(false);
            } else {
                resolve(result[0]);
            }
        });
    });
}

async function getStudentById(studentId){
    return new Promise((resolve, reject) => {
        studentDB.getStudentById(studentId, (err, result) => {
            if (err) {
                reject(false);
            } else {
                resolve(result[0]);
            }
        });
    });
}

module.exports ={
    getStudents,
    getStudentByName,
    getStudentById
}