const fs = require("fs");
const htmlValidator = require("html-validator");
const cssValidator = require("css-validator");
const decompress = require('decompress');
const {getStudentByName} = require("../student/studentservice");
const {getExerciseBySlug} = require("../exercise/exerciseService");
const {v4} = require('uuid');
const cheerio = require('cheerio');
const validationDB = require("../../db/queries/validationQueries")
const faultDB = require("../../db/queries/faultQueries")
const plagiarism = require("../../utils/plagiarism")
const {createScreenshots} = require("../../utils/visualRegression");
const {findIndexHtml} = require("../../utils/htmlFinder");

let errors = [];
let totalScore = 100;
let exercise = null;
let student = null;
let validation = {};


function faultHandler(message, line, score, file) {
    console.log(message, line, score, file)
    errors.push({
        message: message,
        line: line,
        file: file,
        penalty: score
    })
    scoreHandler(score);
    faultDB.createFault(validation.id, message, score, file, line);
}

function scoreHandler(score) {
    totalScore + score >= 0 ? totalScore += score : totalScore = 0;
    validationDB.updateValidationScore(validation.id, totalScore, (err, result) => {
    })
}

async function validateExercise(req, res) {
    await handleFileName(req, res);
    errors = [];
    totalScore = 100;
    const id = v4()

    validationDB.createValidation(`dist/validate/${id}`, 100, exercise.id, student.id, (err, result) => {
        validation.id = result;
        decompress(req.file.path, `dist/validate/${id}`).then(async files => {
                for (let file of files) {
                    if (!file.path.includes("__MACOSX")) {
                        if (file.path.includes(".html")) {
                            await validateHTML(id, file);
                            await getTotalDivs(id, file)
                            await checkHTMLForInlineCSS(id, file)
                        }
                        if (file.path.includes(".css") && !file.path.includes("reset.css")) {
                            await validateCSS(id, file)
                            await checkBannedOrNeeded(id, file)
                        }
                    }
                }
                checkPlagiarism(`dist/validate/${id}`, student.id);
                await createStudentScreens(`/dist/validate/${id}`, id);
                const allPlagiarism = await plagiarism.getProjectPlagiarism(validation.id);


                res.status(200).json({
                    id: validation.id,
                    score: totalScore,
                    warnings: errors,
                    plagiarism: allPlagiarism,
                    images: [{
                        submittedImage: `/students/${id}/screenshot_360x460_student.png`,
                        solutionImage: `/solutions/${exercise.slug}/screenshot_360x460_solution.png`,
                        resolution: "360x460"
                    },
                        {
                            submittedImage: `/students/${id}/screenshot_768x1024_student.png`,
                            solutionImage: `/solutions/${exercise.slug}/screenshot_768x1024_solution.png`,
                            resolution: "768x1024"
                        },
                        {
                            submittedImage: `/students/${id}/screenshot_1366x768_student.png`,
                            solutionImage: `/solutions/${exercise.slug}/screenshot_1366x768_solution.png`,
                            resolution: "1366x768"
                        },
                        {
                            submittedImage: `/students/${id}/screenshot_1920x1080_student.png`,
                            solutionImage: `/solutions/${exercise.slug}/screenshot_1920x1080_solution.png`,
                            resolution: "1920x1080"
                        }]
                })
            }
        )
    })
}

async function handleFileName(req, res) {
    if (!req.file || !req.file.mimetype.includes("zip")) {
        res.status(400).send("Only zip files are allowed")
    }

    const uploadedFilename = req.file.originalname;
    const regex = /^(.+)-(.+)-(.+)\.zip$/;
    const match = uploadedFilename.match(regex);
    const exerciseName = match[1];
    const firstName = match[2];
    const lastName = match[3];
    if (!match) {
        return res.status(400).send("Invalid file name structure. Please use EXERCISENAME-FIRSTNAME-LASTNAME.zip format.");
    }

    exercise = await getExerciseBySlug(exerciseName);
    student = await getStudentByName(firstName, lastName);

    if (!exercise) {
        return res.status(404).send("Exercise was not found. Use the correct exercise name (EXERCISENAME-FIRSTNAME-LASTNAME.zip).");
    }

    if (!student) {
        return res.status(404).send("Student was not found. Use the correct exercise name (EXERCISENAME-FIRSTNAME-LASTNAME.zip).");
    }
}

async function createStudentScreens(filepath, id) {
    const indexPath = findIndexHtml(`${__dirname}/../../${filepath}`)
    await createScreenshots(`${indexPath}`, `${__dirname}/../../public/students/${id}/`, false);
}


async function validateHTML(id, html) {
    const data = fs.readFileSync(__dirname + `/../../dist/validate/${id}/${html.path}`, 'utf8')
    const options = {
        validator: 'http://html5.validator.nu',
        format: 'json',
        data
    }
    try {
        const result = await htmlValidator(options)
        if (result.messages.length > 0) {
            for (let fault of result.messages) {
                const splitLine = data.split('\n');
                const splitLineData = splitLine[fault.lastLine - 1];
                faultHandler(fault.message, `${fault.lastLine}: ${splitLineData}`, -10, html.path)
            }
        }
    } catch (error) {
        console.error(error)
    }
}

function checkHTMLForInlineCSS(id, html) {
    const data = fs.readFileSync(__dirname + `/../../dist/validate/${id}/${html.path}`, 'utf8')
    const $ = cheerio.load(data, {
        xmlMode: true,
        withStartIndices: true
    });

    $('[style]').each((index, element) => {
        const elementStartIndex = element.startIndex;
        const message = "You can't have inline css, think about adding it to the css file";
        const linesBefore = data.substring(0, elementStartIndex).split('\n');
        const splitLine = data.split('\n');
        const splitLineData = splitLine[linesBefore.length - 1];
        faultHandler(message, `${linesBefore.length}: ${splitLineData}`, -10, html.path);
    });
}


async function validateCSS(id, css) {
    const cssData = fs.readFileSync(__dirname + `/../../dist/validate/${id}/${css.path}`, 'utf8')
    cssValidator(cssData, ((err, result) => {
            if (!result.validity) {
                for (let error of result.errors) {
                    const splitLine = cssData.split('\n');
                    const splitLineData = splitLine[error.line - 1];
                    faultHandler(error.errortype, `${error.line}: ${splitLineData}`, -10, css.path)
                }
            }
        }
    ))
}

function getTotalDivs(id, html) {
    const htmlFile = fs.readFileSync(__dirname + `/../../dist/validate/${id}/${html.path}`, 'utf8')
    const $ = cheerio.load(htmlFile);
    const divs = $('div');
    const divCount = divs.length;
}

function checkBannedOrNeeded(id, css) {
    const cssData = fs.readFileSync(__dirname + `/../../dist/validate/${id}/${css.path}`, 'utf8')
    const lines = cssData.split('\n');
    console.log(exercise)


    for (let i = 0; i < lines.length; i++) {
        if (lines[i].includes('px')) {

            faultHandler("Px are not allowed in the exercise, try to use rem instead", `${i + 1}: ${lines[i]}`, -5, css.path);
        }

        const bannedValues = JSON.parse(exercise.banned);
        const lineWithoutSpaces = lines[i].replace(/\s+/g, '');
        for (const property of bannedValues) {
            const propertyWithoutSpaces = property.replace(/\s+/g, '');
            if (lineWithoutSpaces.includes(propertyWithoutSpaces)) {
                faultHandler(`"${property}" should NOT be used in this exercise.`, `${i + 1}: ${lines[i]}`, -10, css.path);
            }
        }
    }

    const expectedValues = JSON.parse(exercise.include);


    for (const property of expectedValues) {
        let foundExpectedValue = false;

        for (let i = 0; i < lines.length; i++) {
            const line = lines[i];
            const lineWithoutSpaces = line.replace(/\s+/g, '');

            const propertyWithoutSpaces = property.replace(/\s+/g, '');

            if (lineWithoutSpaces.includes(propertyWithoutSpaces)) {
                foundExpectedValue = true;
                break;
            }
        }
        if (!foundExpectedValue) {
            faultHandler(`"${property}" should be used in this exercise.`, null, -10, css.path);
        }
    }


}

function checkPlagiarism(filepath, userID) {
    validationDB.getValidationsByExercise(exercise.id, async (err, result) => {
        for (const compareValidation of result) {
            if (compareValidation.userID !== userID) {
                await plagiarism.compareProjects(validation.id, filepath, compareValidation.id, compareValidation.path)
            }
        }

    })
}




module.exports = {
    validateExercise
}