const fs = require('fs');
const path = require('path');

function findIndexHtml(directory) {
    const files = fs.readdirSync(directory);

    for (const file of files) {
        const filePath = path.join(directory, file);
        const stats = fs.statSync(filePath);

        if (stats.isDirectory()) {
            const result = findIndexHtml(filePath);
            if (result) {
                return result;
            }
        } else if (file.toLowerCase() === 'index.html') {
            return filePath;
        }
    }

    return null;
}

module.exports ={
    findIndexHtml
}