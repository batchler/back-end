const fs = require('fs');
const path = require('path');
const stringSimilarity = require('string-similarity');
const plagiarismDB = require("../db/queries/plagiarismQueries")
const validationDB = require("../db/queries/validationQueries");
const {getStudentById} = require("../services/student/studentservice");

function calculateSimilarityPercentage(file1, file2) {
    const similarity = stringSimilarity.compareTwoStrings(file1, file2);
    return similarity * 100;
}

async function compareProjects(validationID, filepath,comparedID, compareFilepath) {
    const files1 = readFilesFromFolder(filepath);
    const files2 = readFilesFromFolder(compareFilepath);

    const htmlFiles1 = files1.filter(file => file.name.endsWith('.html'));
    const cssFiles1 = files1.filter(file => file.name.endsWith('.css'));
    const htmlFiles2 = files2.filter(file => file.name.endsWith('.html'));
    const cssFiles2 = files2.filter(file => file.name.endsWith('.css'));

    let totalHtmlSimilarity = 0;
    let totalCssSimilarity = 0;
    let totalHtmlFiles = 0;
    let totalCssFiles = 0;


    htmlFiles1.forEach(file1 => {
        htmlFiles2.forEach(file2 => {
            const similarityPercentage = calculateSimilarityPercentage(file1.contents, file2.contents);
            totalHtmlSimilarity += similarityPercentage;
            totalHtmlFiles++;
            plagiarismDB.createPlagiarism(validationID, file1.name,comparedID, file2.name, similarityPercentage.toFixed(2))
        });
    });


    cssFiles1.forEach(file1 => {
        cssFiles2.forEach(file2 => {
            if (file1.name !== 'reset.css' && file2.name !== 'reset.css') {
                const similarityPercentage = calculateSimilarityPercentage(file1.contents, file2.contents);
                totalCssSimilarity += similarityPercentage;
                totalCssFiles++;
                plagiarismDB.createPlagiarism(validationID, file1.name,comparedID, file2.name, similarityPercentage.toFixed(2))
            }
        });
    });

}

function readFilesFromFolder(folderPath) {
    const files = [];

    function readFilesRecursively(dir) {
        const fileNames = fs.readdirSync(dir);

        fileNames.forEach(fileName => {
            if (fileName === '__MACOSX') {
                return;
            }
            const filePath = path.join(dir, fileName);
            const fileStats = fs.statSync(filePath);

            if (fileStats.isDirectory()) {
                readFilesRecursively(filePath);
            } else if (fileName.endsWith('.html') || fileName.endsWith('.css')) {
                const fileContent = fs.readFileSync(filePath, 'utf8');
                files.push({
                    name: fileName,
                    contents: fileContent,
                });

            }
        });
    }

    readFilesRecursively(path.join(__dirname, `/../${folderPath}`));

    return files;
}


function getProjectPlagiarism(studentValidationID){
    return new Promise((resolve, reject) => { plagiarismDB.getPlagiarismsByValidationID(studentValidationID, async (err, result) => {
        if (err) {
            return null
        }
        const groupedComparisons = {};
        const transformedResult = [];


        for (const comparison of result) {
            const validationID = comparison.validationID;
            if (!groupedComparisons[validationID]) {
                groupedComparisons[validationID] = {
                    validationID: validationID,
                    averagePercentage: 0,
                    student: await getValidationStudent(validationID),
                    files: [],
                };
            }

            groupedComparisons[validationID].averagePercentage += comparison.percentage;
            groupedComparisons[validationID].files.push({
                student: await getValidationStudent(comparison.comparedID),
                filename: comparison.filename,
                comparedID: comparison.comparedID,
                comparedFilename: comparison.comparedFilename,
                percentage: comparison.percentage,
            });
        }

        transformedResult.push(...Object.values(groupedComparisons).map(group => {
            const totalComparisons = group.files.length;
            group.averagePercentage /= totalComparisons;
            return group;
        }));


        resolve(transformedResult);
    });
    })
}

async function getValidationStudent(validationID) {
    try {
        const result = await new Promise((resolve, reject) => {
            validationDB.getValidationById(validationID, (err, result) => {
                if (err) {
                    reject(err);
                } else {
                    resolve(result);
                }
            });
        });

        if (result && result.length > 0) {
            const userID = result[0].userID;
            const student = await getStudentById(userID);
            return student;
        } else {
            throw new Error('Validation not found');
        }
    } catch (error) {
        console.error('Error in getValidationStudent:', error);
        throw error;
    }
}


module.exports ={
    compareProjects,
    getProjectPlagiarism
}


