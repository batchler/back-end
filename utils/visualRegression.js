const puppeteer = require('puppeteer');
const fs = require('fs');
const {dirname} = require("path");



async function captureScreenshot(url, filename, width, height) {
    const directory = dirname(filename);
    console.log(url)
    if (!fs.existsSync(directory)) {
        console.log("Creating directory:", directory);
        await fs.promises.mkdir(directory, { recursive: true });
    }
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    await page.goto(`file://${url}`);
    await page.setViewport({ width, height });

    const bodyHandle = await page.$('body');
    const { height: contentHeight } = await bodyHandle.boundingBox();
    await page.setViewport({width: parseInt(width), height: parseInt(contentHeight)});
    await bodyHandle.dispose();

    await page.waitForTimeout(1000); // Adjust the delay as needed for your content to load

    const screenshot = await page.screenshot();
    await browser.close();

    fs.writeFileSync(filename, screenshot);
}

async function createScreenshots(url,saveURL, isSolution) {
    const resolutions = [
        { width: 360, height: 460 },
        { width: 768, height: 1024 },
        { width: 1366, height: 768 },
        { width: 1920, height: 1080 }
    ];

    for (const resolution of resolutions) {
        const filenamePrefix = `screenshot_${resolution.width}x${resolution.height}`;
        await captureScreenshot(`${url}`, `${saveURL}${filenamePrefix}_${isSolution?"solution":"student"}.png`, resolution.width, resolution.height);
    }
}

module.exports = {
    createScreenshots
}





