const { body, validationResult } = require('express-validator');


const validateExercise = [
    body('name').notEmpty().withMessage('Name is required'),
    body('slug').notEmpty().withMessage('Slug is required'),
    body('path').notEmpty().withMessage('Path is required'),
    body('fonts').notEmpty().withMessage('Fonts is required'),
    body('colors').notEmpty().withMessage('Colors is required'),
    body('banned').notEmpty().withMessage('Banned is required'),
    body('include').notEmpty().withMessage('Include is required')
];


const validate = (req, res, next) => {
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({ errors: errors.array() });
    }
    next();
};

module.exports = {
    validateExercise,
    validate,
};